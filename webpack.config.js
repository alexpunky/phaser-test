const webpack = require("webpack");
const merge = require('webpack-merge');
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

const devMode = process.env.NODE_ENV !== 'production'

let config = {
    entry: "./src/index.js",
    stats: {
        children: false,
    },
    output: {
        path: path.resolve(__dirname, "./public"),
        filename: "./bundle.js"
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader"
        },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            }]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "styles.css"
        })
    ],
    devServer: {
        contentBase: path.resolve(__dirname, "./public"),
        historyApiFallback: true,
        inline: true,
        open: true,
        hot: true
    },
    devtool: "eval-source-map"
}


const env = process.env.NODE_ENV

config.mode = env || 'development';

/*if (config.mode === 'production') {
    module.exports = merge(config, {
        optimization: {
            minimize: true
        }
    });
} else if (config.mode === 'development') {
    model.exports = merge(config, {
        devtool: 'source-map',
        plugins: [

            new UglifyJSPlugin({
                sourceMap: false
            })
        ],
    });
}*/

module.exports = config;
